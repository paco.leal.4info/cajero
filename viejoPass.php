<?php
session_start();
include 'autoload.php';
Sesion::iniciada();
$id = Sesion::get('usuario', 'id');
$html = New HTML();
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$html->header($titulo);
$html->menu();
$usuario = New Usuario();
$usuario = $usuario->getID($id);
$usuario = $usuario[0];
//unset($usuario['id']);
//$validar = New Validar($usuario);
?>
<div class="container">
<?php
if ($_POST) {
	//echo "<h1>Recibido</h1>";
	if ($usuario['password'] == md5($_POST['old_password'])) {
		$_SESSION['paso1'] = true;
		header("Location: nuevoPass.php");
	} else {
        Sesion::mensaje('La contraseña es incorrecta', 'danger');
		header("Location: viejoPass.php");
	}
}
$html->mensaje();
include 'includes/formPass.php';
?>
</div>
<?php
$html->pie();
?>