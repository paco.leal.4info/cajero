<?php

//include ('lib/dbpdo.php');

class Movimiento extends DBPDO 
{
	private $table = "movimientos";

	public function getTable()
	{
		return $this->table;
	}

	public function insert($params)
	{
	    if ($user = Sesion::get('usuario')) {
            $params['user_id'] = $user['id'];
        } else {
            $params['user_id'] = $_SESSION['id'];
        }

		if ($params['tipo_id'] != 1) {
			$params['cantidad'] *= -1;
		}
		return parent::insert($this->validateParams($params));
	}

	private function validateParams($params)
	{
		//Aquí se hace la validación

		return $params;
	}
	
}