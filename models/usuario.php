<?php

//include ('lib/dbpdo.php');

class Usuario extends DBPDO 
{
	private $table = "usuarios";

	public function getTable()
	{
		return $this->table;
	}

	public function insert($params)
	{
		$params['rol'] = 'usuario';
		$params['credito'] = 0;
        if (isset($params['avatar'])) {
            $params['avatar'] = '/' . $params['avatar'];
        }
		return parent::insert($this->validateParams($params));
	}

	private function validateParams($params)
	{
		//Aquí se hace la validación

		return $params;
	}
	
	public function actualizar($params) {
	    $id = Sesion::get('usuario', 'id');
	    if (isset($params['password'])) {
            $params['password'] = md5($params['password']);
            unset($params['password2']);
        } elseif (isset($params['avatar'])) {
	        $params['avatar'] = '/' . $params['avatar'];
        }

		return parent::update($params, ['id' => $id]);
	}
}