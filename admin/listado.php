<?php
session_start();
include '../autoload.php';
Sesion::iniciada();
if (Sesion::is_admin()):
    $usuarios = New Usuario();
    $usuarios = $usuarios->all();
    $html = New HTML();
    $titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
    $html->header($titulo);
    $html->menu();
    //var_dump($_SESSION['mensaje']['status']);
    ?>
    <div class="container">
        <?php $html->mensaje(); ?>
        <div class="panel panel-default">
            <div class="panel-heading">Usuarios</div>
            <div class="panel-body">
                <?php if ($usuarios): ?>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Email</th>
                            <th>Rol</th>
                            <th>Crédito</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($usuarios as $usuario): ?>
                            <tr>
                                <td><?= $usuario['id'] ?></td>
                                <td><?= $usuario['nombre'] ?></td>
                                <td><?= $usuario['apellidos'] ?></td>
                                <td><?= $usuario['email'] ?></td>
                                <td><?= $usuario['rol'] ?></td>
                                <td><?= $usuario['credito'] ?></td>
                                <td><a href="/editar.php?id=<?= $usuario['id'] ?>" class='btn btn-warning'><span class='glyphicon glyphicon-pencil'></span></a></td>
                                <td><a href="/admin/listado.php?id=<?= $usuario['id'] ?>" class='btn btn-danger'><span class='glyphicon glyphicon-remove'></span></a></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <p>No hay usuarios en la cuenta</p>
                <?php endif ?>
            </div>
        </div>
    </div>
    <?php $html->pie();
endif;