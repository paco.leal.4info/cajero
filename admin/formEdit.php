<div class="panel panel-primary">
	<div class="panel-heading">
		<h1>Editando el usuario
			<?php if ($_GET['id'] == $user['id']): ?>
			<a href="cambiaImg.php" title="Cambiar Avatar"><img src="<?= $user['avatar'] ?>" alt="avatar" class="img-circle welcome pull-right"></img></a>
			<?php endif ?>
		</h1>
	</div>
	<div class="panel-body">
		<div class="container-fluid">
			<form action='#' method="post" accept-charset="utf-8" class="form-horizontal">
				<div class="form-group">
					Nombre: <input type="text" name="nombre" class="form-control" <?php ($_POST || $_GET)? $validar->mostrar_campo('nombre') : ''; ?>>
					<?php (isset($errores['nombre']))? $validar->mostrar_errores_campo('nombre', $errores) : ''; ?>
					<br>
				</div>
				<div class="form-group">
					Apellidos: <input type="text" name="apellidos" class="form-control" <?php ($_POST || $_GET)? $validar->mostrar_campo('apellidos') : '';  ?>>
					<?php (isset($errores['apellidos']))? $validar->mostrar_errores_campo('apellidos', $errores) : ''; ?>
					<br>
				</div>
				<div class="form-group">
					Email: <input type="email" name="email" class="form-control" <?php ($_POST || $_GET)? $validar->mostrar_campo('email') : '';  ?>>
					<?php (isset($errores['email']))? $validar->mostrar_errores_campo('email', $errores) : ''; ?>
					<br>
				</div>
				<?php if ($user['rol'] == 'admin'): ?>
					<div class="form-group">
						Rol: <input type="text" name="rol" class="form-control" <?php ($_POST || $_GET)? $validar->mostrar_campo('rol') : '';  ?>><br>
						<?php (isset($errores['rol']))? $validar->mostrar_errores_campo('rol', $errores) : ''; ?>
					</div>
					<div class="form-group">
						Crédito: <input type="text" name="credito" class="form-control" <?php ($_POST || $_GET)? $validar->mostrar_campo('credito') : '';  ?>><br><?php (isset($errores['credito']))? $validar->mostrar_errores_campo('credito', $errores) : ''; ?>
					</div>
				<?php endif ?>
				<?php if ($_GET['id'] == $user['id']): ?>
					<a href="/viejoPass.php" title="Cambiar Contraseña" class='btn btn-warning pull-left'>Cambiar Contraseña</a>
				<?php endif ?>
				<input type="submit" value="Editar" class="btn btn-info pull-right">
			</form>
		</div>
	</div>
</div>