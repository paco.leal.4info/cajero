<?php 
//spl_autoload_register('includes');
spl_autoload_register('lib');
//spl_autoload_register('models');
/*
function includes($clases)
{
	include_once 'includes/'. $clase . '.php';
}
*/
//$ruta = '~/Escritorio/docker/proyecto1/extra/'

function lib($clase)
{
	if (file_exists('lib/' . strtolower($clase) . '.php')) {
		include_once 'lib/' . strtolower($clase) . '.php';
	} elseif (file_exists('../lib/' . strtolower($clase) . '.php')) {
		include_once '../lib/' . strtolower($clase) . '.php';
	} elseif (file_exists('models/' . strtolower($clase) . '.php')) {
		include_once 'models/' . strtolower($clase) . '.php';
	} elseif (file_exists('../models/' . strtolower($clase) . '.php')) {
		include_once '../models/' . strtolower($clase) . '.php';
	}
}
/*
function models($clase)
{
	if (file_exists('models/' . strtolower($clase) . '.php')) {
		include_once 'models/' . strtolower($clase) . '.php';
	}
}*/

?>