<?php
session_start();
include 'autoload.php';
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$html = New HTML();
$html->header($titulo);
$html->menu();
//echo $errores['nombre'];
?>
<div class="container">
<?php
$html->mensaje();
if ($_POST) {
	$usuario = new Usuario();
	//var_dump($usuario->all());
	$_POST['password'] = md5($_POST['password']);
	if ($user = $usuario->getUser($_POST)) {
        Sesion::usuario($user);
        Sesion::mensaje("Bienvenido, " . $_SESSION['usuario']['nombre'] . ' <img src="' .
            $_SESSION['usuario']['avatar'] .
            '" alt="avatar" class="img-circle align-self-end welcome"></img>', 'success');
        //var_dump(Sesion::get('usuario'));
        //echo Sesion::get('usuario', 'nombre');
        header("Location: welcome.php");
	} else {
        Sesion::mensaje("El usuario o la contraseña son incorrectos", 'danger');
		include 'includes/formLogin.php';
	}
} else {
	include 'includes/formLogin.php';
}
?>
</div>
<?php $html->pie();?>