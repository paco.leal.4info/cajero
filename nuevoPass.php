<?php
session_start();
include 'autoload.php';
Sesion::iniciada();
if ($_SESSION['paso1'] != true) {
	header('Location: viejoPass.php');
}
$html = New HTML();
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$html->header($titulo);
$html->menu();
?>
<div class="container">
<?php
$html->mensaje();
if ($_POST) {
	//echo "<h1>Recibido</h1>";
	$validar = New Validar($_POST);
	$errores = $validar->errores();
	if (empty($errores)) {
		//var_dump($errores);
		$usuario = new Usuario();
		if ($usuario->actualizar($_POST)) {
			$_SESSION['paso1'] = false;
            Sesion::mensaje('La contraseña se ha editado correctamente', 'success');
			header("Location: /welcome.php");
		} else {
            Sesion::mensaje('Algo ha fallado, inténtalo de nuevo más tarde', 'danger');
		}
	} else {
        Sesion::mensaje('La nueva contraseña no es válida, inténtalo de nuevo', 'danger');
    }
} elseif ($_GET) {
	unset($usuario['id']);
	$validar = New Validar($usuario);
}
include 'includes/formPass2.php';
?>
</div>
<?php
$html->pie();
?>