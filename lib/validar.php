<?php 

class Validar
{
	public $errores = [];
	private $password2;
	private $parametros;

	public function __construct($params = null, $files = null)
	{

		if (isset($params['password2'])) {
			$this->password2 = $params['password2'];
			unset($params['password2']);
		}

		if ($params) {
			$this->parametros = $params;

			foreach ($params as $key => $value) {
				$this->$key($value);
			}
		}
		

		if ($files) {
			foreach ($files as $key => $value) {
				$this->$key($value);
			}
		}
	}

	private function name($name)
	{
		//validamos caracteres que puedan afectar a la bbdd
	}

	private function type($type)
	{
		if ($type != 'image/png' && $type != 'image/jpg' && $type != 'image/jpeg') {
			$this->errores['avatar'] = 'El formato debe ser png o jpg';
		}
	}

	private function tmp_name()
	{

	}

	private function error($error)
	{
		if ($error != 0) {
			$this->errores['avatar'] = 'Ha ocurrido un error';
		}
	}

	private function size($size)
	{
		if ($size > 20000) {
			$this->errores['avatar'] = 'Solo se permiten imágenes de hasta 20KB';
		}
	}

	private function avatar($img)
	{

	}

	private function rol($rol = 'usuario')
	{
		if ($rol != 'admin' && $rol != 'usuario' && $rol != '') {
			$this->errores['rol'] = 'Solo se admiten los roles de usuario y admin';
		}
	}

	private function credito($credito)
	{
		if ((!is_int($credito)) || $credito < 0) {
			$this->errores['credito'] = 'Debe ser un entero positivo';
		}
	}

	private function nombre($nombre)
	{
		$erNombre= '/^([A-ZÑÁÉÍÓÚ][a-zñáéíóúü]{2,11}\s?([dD]e(l?)\s)?([lL]os\s)?([lL]a\s)?){1,4}$/i';

		$nombre = ucwords($nombre);
		if (strlen($nombre) < 3) {
			$this->errores['nombre'] = 'El nombre es demasiado corto';
		} elseif (!preg_match($erNombre, $nombre)) {
			$this->errores['nombre'] = 'El nombre contiene caracteres no válidos';
		}
	}

	private function apellidos($apellidos)
	{
		$erApellidos = '/^([A-ZÑÁÉÍÓÚ][a-zñáéíóúü]{2,31}\s?([dD]e(l?)\s)?([lL]os\s)?([lL]a(s?)\s)?){2,4}$/i';
		
		$apellidos = ucwords($apellidos);
		if (strlen($apellidos) < 7) {
			$this->errores['apellidos'] = 'Los apellidos son demasiado cortos';
		} elseif (!preg_match($erApellidos, $apellidos)) {
			$this->errores['apellidos'] = 'Los apellidos contienen caracteres no válidos';
		}
	}

	private function email($email)
	{
		$erEmail = '/^([\w-_.]{2,})@(\w{2,}).([a-z]{2,3})$/';
		if (strlen($email) < 6) {
			$this->errores['email'] = 'El email es demasiado corto';
		} elseif (!preg_match($erEmail, $email)) {
			$this->errores['email'] = 'El formato del email no es válido';
		}
	}

	private function password($password)
	{
		$erPassword = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*?&\-_]{4,}$/';

		if (!empty($password) || !empty($this->password2)) {
				
			if (strlen($password) < 4) {
				$this->errores['password'] = 'La contraseña debe tener al menos 4 caracteres';
			} elseif ($password == $this->password2) {

				if (!preg_match($erPassword, $password)) {

					$this->errores['password'] = 'La contraseña debe contener al menos una mayúscula, minúscula y un dígito';
					// Error de regex
				}

			} else {

				$this->errores['password'] = 'Las contraseñas deben ser iguales';
			}
			
		} else {

			$this->errores['password'] = 'Por favor rellena ambas contraseñas';
		}
	}

	public function errores()
	{
		return $this->errores;
	}

	public function mostrar_campo($campo)
	{
    	if(isset($this->parametros["$campo"])){
   			echo ' value="' . $this->parametros["$campo"] . '"';
		} elseif (isset($usuario["$campo"])) {
			echo ' value="' . $usuario["$campo"] . '"';
		}
	}

	public function mostrar_errores_campo($campo, $errores){
		if(isset($errores[$campo])){
			echo '<span class="text-danger"><small>' . $errores[$campo] . '</small></span>';
		}
	}

}