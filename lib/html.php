<?php 

class HTML
{

	public function header($titulo = 'Cajero')
	{
		?> 
		<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<title><?= $titulo ?></title>
			<link rel="stylesheet" href="">
			<!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

			<!-- jQuery library -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<link rel="stylesheet" type="text/css" href="/estilos/style.css">
			<!-- Latest compiled JavaScript -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		</head>
		<body>
		<?php
	}

	public function menu()
	{
		?>
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">Cajero</a>
					</div>
					<ul class="nav navbar-nav">
						<?php if (isset($_SESSION['usuario']['id'])): ?>
							<li><a href="/operaciones.php">Operaciones</a></li>
							<?php if ($_SESSION['usuario']['rol'] == 'admin'): ?>
								<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Administración <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="/admin/listado.php">Lista de Usuarios</a></li>
										<!--
										<li><a href="#">Page 1-2</a></li>
										<li><a href="#">Page 1-3</a></li>-->
									</ul>
								</li>
							<?php endif;
						endif ?>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<?php if (!isset($_SESSION['usuario']['id'])): ?>
							<li><a href="/registro.php"><span class="glyphicon glyphicon-user"></span> Registrarse</a></li>
							<li><a href="/login.php"><span class="glyphicon glyphicon-log-in"></span> Iniciar Sesion</a></li>
						<?php else: ?>
							<li><a href="/editar.php?id=<?= $_SESSION['usuario']['id'] ?>"><img src="<?= $_SESSION['usuario']['avatar'] ?>" alt="avatar" height="20px" width="20px" class="img-circle"></img> <?= $_SESSION['usuario']['nombre'] ?></a></li>
							<li><a href="/logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar Sesion</a></li>
						<?php endif ?>
					</ul>
				</div>
			</nav>
		<?php
	}

	public function pie()
	{
		?>
		 <footer class="footer">
    		<div class="container">
     		    <p class="text-muted text-center">Fco José Leal Ballesta © 2018 Copyright:
        <a href="http://www.iescierva.net/"> IES Ingeniero de la Cierva </a>
    </p>
     		</div>
   		 </footer>
		</body>
		</html>
		<?php
	}

	public function mensaje()
	{
		if (isset($_SESSION['mensaje']['status']) && $_SESSION['mensaje']['status'] == true): ?>
			<div class="panel panel-<?= $_SESSION['mensaje']['tipo'] ?>" id='mensaje'>
				<div class="panel-heading">
					<button type="button" class="close" data-target="mensaje" data-dismiss="panel">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class='text-center'><?= $_SESSION['mensaje']['contenido'] ?></h3>
				</div>
			</div>
			<?php
		endif;
		$_SESSION['mensaje']['status'] = false;
        $_SESSION['mensaje']['contenido'] = '';
        $_SESSION['mensaje']['tipo'] = '';
    }

    public function total($total) {
        switch ($total) {
            case ($total < 0):
                return "class='danger'";
                break;
            case ($total >= 0 && $total <= 100):
                return "class='warning'";
                break;
            default:
                return "class='success'";
                break;
        }
    }
}

?>