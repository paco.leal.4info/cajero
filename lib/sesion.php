<?php

class Sesion
{
    protected static $usuario = null;

    public static function mensaje($contenido, $tipo)
    {
        $_SESSION['mensaje']['contenido'] = $contenido;
        $_SESSION['mensaje']['tipo'] = $tipo;
        $_SESSION['mensaje']['status'] = true;
    }

    public static function is_admin()
    {
        if ($_SESSION['usuario']['rol'] != 'admin') {
            self::mensaje('Para eso necesitas permisos de administrador', 'danger');
            header("Location: /welcome.php");
            return false;
        }
        return true;
    }

    public static function iniciada()
    {
        if (!isset($_SESSION['usuario']['rol'])) {
            header("Location: /login.php");
            return false;
        }
        return true;
    }

    public static function destroy($destino = 'index.php')
    {
        session_destroy();
        header("Location: $destino");
    }

    public static function usuario($user)
    {
        $_SESSION['usuario']['nombre'] = $user['nombre'];
        $_SESSION['usuario']['id'] = $user['id'];
        $_SESSION['usuario']['rol'] = $user['rol'];
        $_SESSION['usuario']['avatar'] = $user['avatar'];
        $_SESSION['usuario']['credito'] = $user['credito'];
        $_SESSION['usuario']['paso1'] = false;
    }

    public static function add($clave, $params)
    {
        foreach ($params as $key => $value) {
            $_SESSION[$clave][$key] = $value;
        }
    }

    public static function get($clave1, $clave2 = null)
    {
        switch ($clave2) {
            case null:
                return $_SESSION[$clave1];
                break;
            default:
                return $_SESSION[$clave1][$clave2];
                break;
        }
    }
}

?>