<?php

class DBPDO
{
	//Datos de la Conexión
	private $host = 'localhost';    //mysql en docker
	private $port = '3306';
	private $user = 'user';
	private $pass = 'user';
	private $dbname = 'cajero';
	private $charset = 'utf8';

	//Contendrá el error en caso de producirse
	public $errors = false;

	//La conexión a la BD
	private $db;

	//Indica en que modo estamos (DEV o PRO)
	public $modeDEV = false;

	//Indica si queremos una conexión persistente o no
	private $persistent = true;

	public function __construct()
	{
		$this->db = $this->connection();
	}

	private function connection()
	{
		$dsn = 'mysql:host=' . $this->host . 
			   ';port=' . $this->port . 
			   ';dbname=' . $this->dbname .
                ';charset=' . $this->charset;

		$options = [ PDO::ATTR_PERSISTENT => $this->persistent,
					 PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
				   ];

		try {

			return new PDO($dsn, $this->user, $this->pass, $options);
		
		} catch(PDOException $e) {

			$this->errors = $e->getMessage();
			if ($this->modeDEV) {
				print_r($this->errors);
			}
		}
	}

	public function getConnection()
	{
		return $this->db;
	}

	public function setDBPassword($pass)
	{
		$this->pass = $pass;
		$this->connection();
	}

	public function setDBHost($host)
	{
		$this->host = $host;
		$this->connection();
	}

	public function setDBPort($port)
	{
		$this->port = $port;
		$this->connection();
	}

	public function setDBUser($user)
	{
		$this->user = $user;
		$this->connection();
	}

	public function setDBName($dbname)
	{
		$this->dbname = $dbname;
		$this->connection();
	}

	public function setDB($data)
	{
		$this->dbname = $data['dbname'];
		$this->host = $data['host'];
		$this->user = $data['user'];
		$this->pass = $data['pass'];
		$this->port = $data['port'];

		$this->connection();
	}

	public function all($limit = 10)
	{
		$prepare = $this->db->prepare('SELECT * 
									   FROM ' . $this->getTable() .
									  ' LIMIT ' . $limit
									 );
		$prepare->execute();

		$this->setQuery($prepare);

		return $prepare->fetchAll(PDO::FETCH_ASSOC);
	}

	public function insert($params)
	{
		if ( ! empty($params)) {

			$fields = '(' . implode(',', array_keys($params)) . ')';

			$values = "(:" . implode(",:", array_keys($params)) . ")";

			$ssql =  'INSERT INTO ' .
					  $this->getTable() . ' ' . $fields . 
					 ' VALUES ' . $values;

			$prepare = $this->db->prepare($ssql);

			$prepare->execute($this->normalizePrepareArray($params));

			$this->setQuery($prepare);

			return $this->db->lastInsertId();

		} else {
			throw new Exception('Los parámetros están vacíos');
		}
	}

	public function setQuery($sql)
	{
		if ($this->modeDEV) {
			$sql->debugDumpParams();
		}
	}

	private function normalizePrepareArray($params)
	{
		foreach ($params as $key => $value) {
			$params[':' . $key] = $value;
			unset($params[$key]);
		}

		return $params;
	}

	public function update($params, $where)
	{
		if ( ! empty($params)) {

			$fields = '';
			foreach ($params as $key => $value) {
				$fields .= $key . ' = :' . $key . ', ';
			}
			$fields = rtrim($fields, ', ');

			$key = key($where);
			$value = $where[$key];

			$ssql = 'UPDATE ' . $this->getTable() . 
				    ' SET ' . $fields .
				    ' WHERE ' . $key . '=' . $value;

			$prepare = $this->db->prepare($ssql);

			$prepare->execute($this->normalizePrepareArray($params));

			$this->setQuery($prepare);

			return true;

		} else {

			throw new Exception('Los parámetros están vacíos');

		}
	}
	
	public function delete($where)
	{

		if ( ! empty($where)) {

			$key = key($where);

			$ssql = 'DELETE FROM ' . $this->getTable() . 
					' WHERE ' . $key . '=:' . $key;

			$prepare = $this->db->prepare($ssql);

			$prepare->execute($this->normalizePrepareArray($where));

			if ($this->setQuery($prepare)) {

				return true;

			} else {

				return false;
			}

		} else {

			throw new Exception('Los parámetros están vacíos');

		}
	}

	public function getID($id)
	{

		$ssql = 'SELECT * FROM ' . $this->getTable() .
				' WHERE id="' . $id . '"';

		$prepare = $this->db->prepare($ssql);
		$prepare->execute();
		return $prepare->fetchAll(PDO::FETCH_ASSOC);

	}

	public function getUser($params)
	{

		$ssql = "SELECT * FROM " . $this->getTable() .
				" WHERE email='$params[email]' AND password='$params[password]'";

		$prepare = $this->db->prepare($ssql);
		$prepare->execute();
		return $prepare->fetch(PDO::FETCH_ASSOC);

	}

	public function getMovimientos($id)
	{

		$ssql = "SELECT * FROM " . $this->getTable() .
				" WHERE user_id='$id' ORDER BY fecha DESC";

		$prepare = $this->db->prepare($ssql);
		$prepare->execute();
		return $prepare->fetchAll(PDO::FETCH_ASSOC);

	}

	public function getTotal($id)
    {
        $ssql = "SELECT SUM(cantidad) FROM " . $this->getTable() .
            " WHERE user_id='$id'";

        $prepare = $this->db->prepare($ssql);
        $prepare->execute();
        return $prepare->fetch(PDO::FETCH_ASSOC);
    }

	public function getTipo()
	{

		$ssql = "SELECT * FROM " . $this->getTable();

		$prepare = $this->db->prepare($ssql);
		$prepare->execute();
		return $prepare->fetchAll(PDO::FETCH_ASSOC);

	}
}
