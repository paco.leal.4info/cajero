
<div class="panel panel-primary">
	<div class="panel-heading">
		<h1>Introduce tu nueva contraseña</h1>
	</div>
	<div class="panel-body">
		<div class="container-fluid">
			<form action='#' method="post" accept-charset="utf-8" class="form-horizontal">
				<div class="form-group">
					Nueva contraseña: <input type="password" name="password" class="form-control"><br>
					<?php (isset($errores['password']))? $validar->mostrar_errores_campo('password', $errores) : ''; ?>
				</div>
				<div class="form-group">
					Repite nueva contraseña: <input type="password" name="password2" class="form-control"><br>
					<input type="submit" value="Enviar" class="btn btn-info pull-right">
				</div>
			</form>
		</div>
	</div>
</div>