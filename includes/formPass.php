<div class='col-md-6 col-md-offset-3'>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1>Edita tu contraseña</h1>
		</div>
		<div class="panel-body">
			<div class="container-fluid">
				<form action='#' method="post" accept-charset="utf-8" class="form-horizontal">
					<div class="form-group">
						Contraseña antigua: <input type="password" name="old_password" class="form-control">
						<?php (isset($errores['old_password']))? $validar->mostrar_errores_campo('old_password', $errores) : ''; ?>
					</div><br>
				</div>
				<input type="submit" value="Enviar" class="btn btn-info pull-right">
			</form>
		</div>
	</div>
</div>
