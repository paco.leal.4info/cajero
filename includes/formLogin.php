
<div class="panel panel-primary">
    <div class="panel-heading">
        <h1>Inicia Sesión</h1>
    </div>
    <div class="panel-body">
        <div class="container-fluid">
            <form action='#' method="post" accept-charset="utf-8" class="form-horizontal">
                <div class="form-group">
                    Email: <input type="email" name="email" class="form-control"><br>
                </div>
                <div class="form-group">
                    Contraseña: <input type="password" name="password" class="form-control"><br>
                </div>
                <input type="submit" value="Enviar" class="btn btn-info pull-right">
            </form>
        </div>
    </div>
</div>