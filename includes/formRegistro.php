<div class="container">
	<?php $html->mensaje(); ?>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1>Regístrate</h1>
		</div>
		<div class="panel-body">
			<div class="container-fluid">
				<form action='#' method="post" accept-charset="utf-8" class="form-horizontal" enctype='multipart/form-data'>
					<div class="form-group">
						Nombre: <input type="text" name="nombre" class="form-control" <?php ($_POST)? $validar->mostrar_campo('nombre'):''; ?>>
						<?php (isset($errores['nombre']))? $validar->mostrar_errores_campo('nombre', $errores) : ''; ?>
						<br>
					</div>
					<div class="form-group">
						Apellidos: <input type="text" name="apellidos" class="form-control" <?php ($_POST)? $validar->mostrar_campo('apellidos'):'';  ?>>
						<?php (isset($errores['apellidos']))? $validar->mostrar_errores_campo('apellidos', $errores) : ''; ?>
						<br>
					</div>
					<div class="form-group">
						Email: <input type="email" name="email" class="form-control" <?php ($_POST)? $validar->mostrar_campo('email'):'';  ?>>
						<?php (isset($errores['email']))? $validar->mostrar_errores_campo('email', $errores) : ''; ?>
						<br>
					</div>
					<div class="form-group">
						Contraseña: <input type="password" name="password" class="form-control"><br>
						<?php (isset($errores['password']))? $validar->mostrar_errores_campo('password', $errores) : ''; ?>
					</div>
					<div class="form-group">
						Repite contraseña: <input type="password" name="password2" class="form-control"><br>
					</div>
					<div class="form-group">
						Avatar: <input type="file" name="avatar" id="avatar">
						<?php (isset($errores['avatar']))? $validar->mostrar_errores_campo('avatar', $errores) : ''; ?>
						<br>
					</div>
					<input type="submit" value="Enviar" class="btn btn-info pull-right">
				</form>
			</div>
		</div>
	</div>
</div>