<?php
session_start();
include 'autoload.php';
Sesion::iniciada();
$user = Sesion::get('usuario');
if ($user['id'] != $_GET['id'] && $user['rol'] != 'admin') {
    Sesion::mensaje('No puedes editar otro usuario', 'danger');
    header("Location: /editar.php?id=$user[id]");
}

$html = New HTML();
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$html->header($titulo);
$html->menu();
$usuario = New Usuario();
$usuario = $usuario->getID($_GET['id']);
$usuario = $usuario[0];
//unset($usuario['id']);
//$validar = New Validar($usuario);
?>
    <div class="container">
<?php
$html->mensaje();
if ($_POST) {
	//echo "<h1>Recibido</h1>";
	$_POST['credito'] = intval($_POST['credito']);
	$validar = New Validar($_POST);
	$errores = $validar->errores();
	if (empty($errores)) {
		//var_dump($errores);
		$usuario = new Usuario();
		if ($usuario->update($_POST, $_GET)) {
		    Sesion::mensaje('El usuario ha sido editado correctamente', 'success');
            if ($_GET['id'] == $user['id']) {
                header("Location: welcome.php");
            } else {
                header("Location: /admin/listado.php");
            }
		} else {
            Sesion::mensaje('Algo ha fallado, inténtalo de nuevo más tarde', 'danger');
		}
	} else {
	    //volver a mostrar el formulario con los errores
    }
} elseif ($_GET) {
	unset($usuario['id']);
	$validar = New Validar($usuario);
}
?>
    <?php include 'admin/formEdit.php'; ?>
</div>
<?php
$html->pie();?>