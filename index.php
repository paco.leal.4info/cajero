<?php session_start();
$_SESSION['mensaje']['status'] = false;
// Ejercicio: cajero

/* Necesitamos: usuarios (nombre, apellido, email, password), transacciones y tipo (de movimiento) en la BBDD
modelos: usuario y transaccion

pantalla de inicio
formulario de transacciones
login y registro

nombre de la BBDD: cajero
*/
include 'autoload.php';
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$html = New HTML();
$html->header($titulo);
$html->menu();
 ?>
<div class="container">
	<?php $html->mensaje(); ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h1 class='text-center'>Bienvenido a la aplicación</h1>
		</div>
		<div class="panel-body">
			<div class="container-fluid">
				<div class='text-muted text-justify'>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium tortor vitae tortor hendrerit, eget placerat leo congue. Proin non tortor pellentesque, cursus neque id, fermentum eros. Integer mattis ipsum a lorem egestas, et semper metus pellentesque. Fusce sit amet aliquet leo, vitae dictum nulla. Aenean eu nibh erat. Nunc non hendrerit risus, quis convallis nisl. Praesent rhoncus purus eget nisi venenatis, sed porta erat volutpat. Morbi aliquam sollicitudin leo, in convallis ante malesuada non. Suspendisse vel accumsan magna. Duis in neque semper, blandit libero efficitur, maximus justo. Proin ornare placerat ex, vel lacinia arcu convallis ac. Phasellus tellus odio, congue id dui pellentesque, tempus viverra ex.</p>

					<p>Nulla rhoncus, ex lobortis feugiat blandit, dui nisi tempus lectus, non dictum urna metus eget tortor. Nullam malesuada felis id sapien aliquet ultricies. Aliquam odio dui, sagittis sit amet metus at, luctus commodo risus. Proin eu enim elementum, scelerisque sapien id, congue tortor. Integer tristique mauris sed lorem ultricies tempus in tempus eros. Suspendisse tincidunt dolor justo. Proin id erat id lectus vulputate ultricies. Donec finibus ipsum eleifend augue fermentum, in condimentum nisi elementum. Morbi sit amet placerat orci. In eu ante lectus. Donec nisi nulla, dictum vel blandit at, tincidunt non metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc at venenatis justo, eu venenatis ante. Phasellus sed sagittis sem, ut rhoncus purus. Cras cursus mauris vitae consectetur auctor. Cras et suscipit nulla.</p>

					<p>Vestibulum sed diam nec risus elementum facilisis. Phasellus eu sem elit. Mauris consectetur libero at mi consectetur consectetur. Praesent ultrices, ante sed luctus rhoncus, mauris lacus tincidunt quam, eu tristique lorem turpis eu tortor. Etiam semper maximus risus in vehicula. Pellentesque venenatis consectetur pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse sollicitudin sapien nisl, eget varius felis consequat in. Aenean maximus sed lorem quis laoreet. Sed fermentum ac risus nec dapibus. Quisque justo felis, venenatis in ipsum at, sodales laoreet arcu. Nulla congue posuere odio eget placerat. Nunc quis hendrerit dui, eu volutpat ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$html->pie();
?>
