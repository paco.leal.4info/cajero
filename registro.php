<?php
include 'autoload.php';
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$html = New HTML();
$html->header($titulo);
$html->menu();

if ($_POST) {
	//echo "<h1>Recibido</h1>";
	if ($_FILES) {
		$validar = New Validar($_POST, $_FILES);
	} else {
		$validar = New Validar($_POST);
	}
	$errores = $validar->errores();
	if (empty($errores)) {
		if ($_FILES) {
			$destino = 'public/img/' . date('dmY-H_i') . '-' . $_FILES['avatar']['name'];
			if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $destino)) {
				$mensaje['tipo'] = 'danger';
				$mensaje['contenido'] = 'Ha ocurrido un error al subir el archivo';
			} else {
				$usuario = new Usuario();
				$_POST['password'] = md5($_POST['password']);
				unset($_POST['password2']);
				$_POST['avatar'] = $destino;
				if ($id = $usuario->insert($_POST)) {
					header("Location: login.php");
				} else {
					$errores[] = 'Algo ha fallado, inténtalo de nuevo más tarde';
					include 'includes/formRegistro.php';
				}
			}
		} else {
			$validar = New Validar($_POST);
            //var_dump($errores);
            $usuario = new Usuario();
            $_POST['password'] = md5($_POST['password']);
            unset($_POST['password2']);
            if ($id = $usuario->insert($_POST)) {
                header("Location: login.php");
            } else {
                $errores[] = 'Algo ha fallado, inténtalo de nuevo más tarde';
                include 'includes/formRegistro.php';
            }
		}
	}
} else {
	include 'includes/formRegistro.php';
}

/*
Falta implementar bien html y la imagen y el patrón singleton
*/
$html->pie();?>