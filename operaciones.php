<?php
session_start();
include 'autoload.php';
Sesion::iniciada();
$html = New HTML();
$movimiento = New Movimiento();
$user = Sesion::get('usuario');
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$total = $movimiento->getTotal($user['id'])['SUM(cantidad)'];
$validar = New Validar();
if ($_POST) {
    $_POST['user_id'] = $user['id'];
    if ($_POST['tipo_id'] == 2) {
        $user['credito'] = (float) $user['credito'];
        $total2 = (float) $total;
        $_POST['cantidad'] = (float) $_POST['cantidad'];
        $diferencial = $user['credito'] + $total2 - $_POST['cantidad'];
        if ($diferencial < 0 ) {
            //var_dump($total);
            Sesion::mensaje('Esa extracción no es posible con tu crédito actual', 'danger');
        } else {
            $movimiento->insert($_POST);
        }
    } else {
        $movimiento->insert($_POST);
    }
}
$total = $movimiento->getTotal($user['id'])['SUM(cantidad)'];
$movimientos = $movimiento->getMovimientos($user['id']);
$paginacion = count($movimientos);  //numero de resultados de MySQL
if (!$_GET) {
    $current_page = 0;
} else {
    $current_page = $_GET['current_page'];
}
$limite = 9;                        //numero de resultados mostrados por página
$max_page = floor($paginacion / $limite);
//echo $max_page;
$tipos = New Tipo();
$tipos = $tipos->getTipo();
//var_dump($tipos);
$html->header($titulo);
$html->menu();
?>
<div class="container">
    <?php $html->mensaje(); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Tus movimientos
            <div class="pull-right">
                <a href="/operaciones.php?current_page=<?= $current_page + 1 ?>" class="<?= ($current_page == $max_page)? 'text-muted disabled' : 'text-dark' ?>"><span class="glyphicon glyphicon-arrow-left"></span></a> &nbsp; <?= $current_page ?> &nbsp;
                <a href="/operaciones.php?current_page=<?= $current_page - 1 ?>" class="<?= ($current_page == 0)? 'text-muted disabled' : 'text-dark' ?>"><span class="glyphicon glyphicon-arrow-right"></span></a>    <!-- muted y disabled si desactivamos -->
            </div>
        </div>
        <div class="panel-body">
            <?php if ($movimientos): ?>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Movimiento</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach (array_reverse($movimientos) as $operacion):
                        $paginacion--;
                        if (($paginacion >= ($current_page+1) * $limite - $limite) && ($paginacion <= ($current_page+1) * $limite)) { ?>
                            <tr <?= ($operacion['cantidad'] < 0) ? "class='danger'" : '' ; ?>>
                                <td><?= $operacion['fecha'] ?></td>
                                <td><?= $tipos[($operacion['tipo_id'] - 1)]['nombre'] ?></td>
                                <td><?= $operacion['cantidad'] ?></td>
                            </tr>
                       <?php }
                        endforeach ?>
                        <tr <?= $html->total($total); ?>>
                            <td colspan="2">Total: </td>
                            <td><?= $total ?></td>
                        </tr>
                    </tbody>
                </table>
            <?php else: ?>
                <p>No hay movimientos en la cuenta</p>
            <?php endif ?>
        </div>
    </div>
    <div class="text-center">
        <form class='form-inline' action='#' method='POST'>
            <div class="form-group">
                <label for="tipo_id">Movimiento:</label>
                <select class="form-control" name="tipo_id">
                    <?php foreach ($tipos as $tipo): ?>
                        <option value="<?= $tipo['id'] ?>"><?= $tipo['nombre'] ?></option>
                    <?php endforeach ?>
                </select>&nbsp;
                <label for="cantidad">Cantidad:</label>
                <input type="text" class="form-control" name="cantidad">
                <input type="submit" class="btn btn-success" value='Enviar'>

            </div>
        </form>
    </div>
</div>

<?php $html->pie();?>
