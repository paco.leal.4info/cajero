<?php
session_start();
include 'autoload.php';
Sesion::iniciada();
$user = Sesion::get('usuario');
$titulo = rtrim(basename($_SERVER['PHP_SELF']), '.php');
$html = New HTML();
$html->header($titulo);
$html->menu();
?>
<div class="container">
<?php
$html->mensaje();
if ($_FILES) {
    echo $_FILES['avatar']['error'];
	//echo "<h1>Recibido</h1>";
	$validar = New Validar(null, $_FILES['avatar']);
	$errores = $validar->errores();
	if (empty($errores)) {
		//var_dump($errores);
		$usuario = new Usuario();
		$destino = 'public/img/' . date('dmY-H_i') . '-' . $_FILES['avatar']['name'];
		//echo $destino;
		if (move_uploaded_file($_FILES['avatar']['tmp_name'], $destino)) {
			if ($usuario->actualizar(['avatar' => $destino])) {
                Sesion::mensaje("Has cambiado tu avatar", "success");
                Sesion::add('usuario', ['avatar' => $destino]);
				header("Location: editar.php?id=" . $user['id']);
			} else {
                Sesion::mensaje('Ha ocurrido un error al conectar con la Base de Datos', 'danger');
			}
		} else {
            Sesion::mensaje('Ha ocurrido un error al subir el archivo', 'danger');
		}
	}
}

/*
Cambiar la bbdd para que acepte la ruta de la imagen del usuario (por defecto /extra/public/img/icono.png)

validar la imagen (comprobamos que es una image, tiene la extensión correcta y no supera el límite de tamaño)

Añadir al formulario de edición la subida de la imagen (se mostrará la imagen a la izquierda del campo de edición)

(Añadirla al menu y la página welcome.php)*/
//var_dump($errores);
?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>Cambia tu avatar <img src="<?= $user['avatar'] ?>" alt="Avatar Actual" class="welcome img-circle pull-right"></img></h1>
        </div>
        <div class="panel-body">
            <div class="container-fluid">
                <form action='#' method="post" accept-charset="utf-8" class="form-horizontal" enctype='multipart/form-data'>
                    <div class="form-group">
                        Avatar: <input type="file" name="avatar" id="avatar">
                        <?php (isset($errores['avatar']))? $validar->mostrar_errores_campo('avatar', $errores) : ''; ?>
                        <br>
                    </div>
                    <input type="submit" value="Enviar" class="btn btn-info pull-right">
                </form>
            </div>
        </div>
    </div>
</div>

<?php $html->pie(); ?>